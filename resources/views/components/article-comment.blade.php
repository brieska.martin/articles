@props(['comment'])

<div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
    <div class="p-6">
        <div class="flex items-center justify-between">
            <div class="text-lg leading-7 font-semibold text-gray-900">
                {{ $comment->author->name }}
            </div>
            <div class="flex items-center">
                <div class="text-gray-600 dark:text-gray-400 text-sm">
                    {{ date('j.n.Y', strtotime($comment->created_at)) }}
                </div>
                @if (Auth::user()->hasRole('admin'))
                    <form class="ml-3" action="{{ route('commentDelete') }}" method="POST">
                        @csrf
                        <input name="_method" type="hidden" value="DELETE">
                        <input name="id" type="hidden" value="{{ $comment->id }}">
                        <a href="#" onclick="event.preventDefault(); this.closest('form').submit();">
                            <svg class=" h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                            </svg>
                        </a>
                    </form>
                @endif
            </div>
        </div>
        <div>
            @if (Auth::user()->hasRole('admin'))
                <form action="{{ route('commentUpdate') }}" method="POST">
                    @csrf
                    <input name="_method" type="hidden" value="PUT">
                    <input type="hidden" name="id" value="{{ $comment->id }}">
                    <input
                        name="commentText"
                        class="form-input border-2 p-2 mt-1 block w-full"
                        value="{{ $comment->text}}"
                    />
                </form>
            @else
                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                    {{ $comment->text }}
                </div>
            @endif

        </div>
        <form action="{{ route('commentCreate') }}" method="POST">
            @csrf
            <input type="hidden" name="parentId" value="{{ $comment->id }}">
            <input type="hidden" name="type" value="comment">
            <input name="commentText" class="form-input border-2 p-2 mt-1 block w-full" placeholder="Odpoveď...">
        </form>
    </div>
    <div class="ml-6 pb-6">
        @foreach ($comment->comments as $subComment)
            <x-article-comment :comment="$subComment" />
        @endforeach
    </div>
</div>
