<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PageController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function() {
    Route::get('/dashboard', [AdminController::class, 'dashboard'])->name('dashboard');

    /*
     * Article routes
     */
    Route::get('/admin/article/{article}', [AdminController::class, 'article'])->name('articleAdmin');
    Route::get('/article/{article}', [ArticleController::class, 'index'])->name('article');

    /*
     * Comment routes
     */
    Route::post('/comment', [CommentController::class, 'create'])->name('commentCreate');
    Route::put('/comment', [CommentController::class, 'update'])->name('commentUpdate');
    Route::delete('/comment', [CommentController::class, 'delete'])->name('commentDelete');
});

require __DIR__.'/auth.php';
