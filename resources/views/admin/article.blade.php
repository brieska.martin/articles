<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Comments for article: {{ $article->name }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @foreach ($article->comments as $comment)
                <x-article-comment :comment="$comment" />
            @endforeach
        </div>
    </div>
</x-app-layout>
