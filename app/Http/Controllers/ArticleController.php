<?php

namespace App\Http\Controllers;

use App\Models\Article;

class ArticleController extends Controller
{
    public function index(Article $article)
    {
        return view('article.index', [
            'article' => $article
        ]);
    }
}
