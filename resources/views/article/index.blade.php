<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $article->name }}
        </h2>
    </x-slot>
    <div class="max-w-6xl mx-auto">
        <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
            <div class="p-6">
                <div class="flex items-center">
                    <div class="text-lg leading-7 font-semibold text-gray-900">
                        {{ $article->name }}
                    </div>
                </div>
                <div>
                    <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                        {{ $article->text }}
                    </div>
                </div>
            </div>
        </div>
        <div class="leading-7 font-semibold text-gray-900">
            Diskusia
        </div>
        <form action="{{route('commentCreate')}}" method="POST">
            @csrf
            <input type="hidden" name="parentId" value="{{ $article->id }}">
            <input type="hidden" name="type" value="article">
            <input name="commentText" class="form-input border-2 p-2 mt-1 block w-full" placeholder="Komentovať...">
        </form>
        @foreach ($article->comments as $comment)
            <x-article-comment :comment="$comment" />
        @endforeach
    </div>
</x-app-layout>
