<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ArticleSeeder::class,
            CreateAdminUserSeeder::class,
            CreateUserSeeder::class,
            PermissionTableSeeder::class
        ]);
    }
}
