<?php

namespace App\Http\Controllers;

use App\Models\Article;

class PageController extends Controller
{
    public function index()
    {
        return view('index', [
            'articles' => Article::all()
        ]);
    }
}
