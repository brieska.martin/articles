<?php

namespace App\Http\Controllers;

use App\Models\Article;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:admin']);
    }

    public function dashboard()
    {
        return view('dashboard', [
            'articles' => Article::all()
        ]);
    }

    public function article(Article $article)
    {
        return view('admin.article', [
            'article' => $article
        ]);
    }
}
