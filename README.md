# Articles

### How to run this project

#### Install dependencies
```shell
npm install
composer install
```

#### Copy .env.example to .env
```shell
cp .env.example .env
```

#### Start database container
```shell
docker-compose up
```

#### Compile
```shell
npm run dev
```

#### Run migrations and seed database
```shell
php artisan migrate:fresh --seed
```

#### Start laravel
```shell
php artisan serve
```

### Test users

**Admin**  
**Email:** admin@admin.com  
**Password:** secret
---
**Simple user**  
**Email:** test@test.com  
**Password:** secret
---
