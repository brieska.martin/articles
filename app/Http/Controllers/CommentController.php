<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCommentRequest;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin',
            [
                'only' => ['update','delete']
            ]
        );
    }

    public function create(StoreCommentRequest $request)
    {
        $comment = new Comment([
            'text' => $request->get('commentText'),
            'user_id' => Auth::id(),
            'commentable_type' => Comment::TYPE[$request->get('type')],
            'commentable_id' => $request->get('parentId')
        ]);
        $comment->save();

        return redirect()->back();
    }

    public function update(StoreCommentRequest $request)
    {
        $comment = Comment::findOrFail($request->get('id'));
        $comment->text = $request->get('commentText');
        $comment->save();

        return redirect()->back();
    }

    public function delete(Request $request)
    {
        Comment::findOrFail($request->get('id'))->delete();
        return redirect()->back();
    }
}
